package uz.bot.spring.telegram.sarvice.impl;

import org.springframework.stereotype.Service;
import uz.bot.spring.telegram.model.domen.Region;
import uz.bot.spring.telegram.model.dto.RegionDto;
import uz.bot.spring.telegram.repository.RegionRepository;
import uz.bot.spring.telegram.sarvice.RegionService;

import java.util.HashSet;
import java.util.Set;

@Service
public class RegionServiceImpl implements RegionService {

    private final RegionRepository regionRepository;

    public RegionServiceImpl(RegionRepository regionRepository) {
        this.regionRepository = regionRepository;
    }

    public Set<Region> findAll(){

        Set<Region>  regions = new HashSet<>();
        regions.addAll(regionRepository.findAll());

        return regions;

    }

    public Region findById(Long id) {

        return regionRepository.findById(id).get();

    }
}
