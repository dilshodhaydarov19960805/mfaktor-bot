package uz.bot.spring.telegram.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uz.bot.spring.telegram.model.domen.Users;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<Users, Long> {

    @Query("select u from Users u where u.id = :id")
    Optional<Users> findById(@Param("id") Long id);

}
