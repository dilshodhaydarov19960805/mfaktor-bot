package uz.bot.spring.telegram.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PositionDto {

    private Long id;

    private String positionNameUz;

    private String positionNameRu;

}
