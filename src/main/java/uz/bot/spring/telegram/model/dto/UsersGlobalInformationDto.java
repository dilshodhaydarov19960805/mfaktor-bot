package uz.bot.spring.telegram.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.bot.spring.telegram.model.domen.UsersGlobalInformation;

import java.util.Locale;
import java.util.Objects;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UsersGlobalInformationDto {

    private Integer userId;
    private Locale lang;
    private int transactionLangI = 0;
    private String callbackQueryId;
    private Integer myMessageId = 0;
    private Long globalId;

    public UsersGlobalInformation userInfoToEntity() {
        UsersGlobalInformation userInfoG = new UsersGlobalInformation();

        userInfoG.setUserId(userId);

        if (lang != null){
            userInfoG.setLang(lang);
        }

        userInfoG.setTransactionLangI(transactionLangI);

        if (callbackQueryId != null){
            userInfoG.setCallbackQueryId(callbackQueryId);
        }
        if (myMessageId != null){
            userInfoG.setMyMessageId(myMessageId);
        }
        if (globalId != null){
            userInfoG.setGlobalId(globalId);
        }

        return userInfoG;
    }

}
