package uz.bot.spring.telegram.sarvice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.bot.spring.telegram.model.domen.Position;
import uz.bot.spring.telegram.repository.PositionRepository;
import uz.bot.spring.telegram.sarvice.PositionService;

import java.util.HashSet;
import java.util.Set;

@Service
public class PositionServiceImpl implements PositionService {

    @Autowired
    private PositionRepository positionRepository;

    public Set<Position> findAll(){

        Set<Position> positions = new HashSet<>();
        positions.addAll(positionRepository.findAll());

        return positions;

    }

    public Position findById(Long id) {

        return positionRepository.findById(id).get();
    }
}
