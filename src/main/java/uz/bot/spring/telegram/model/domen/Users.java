package uz.bot.spring.telegram.model.domen;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import uz.bot.spring.telegram.model.dto.UserDto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users_info")
public class Users implements Serializable {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "region")
    private String region;

    @Column(name = "type_of_activity")
    private String typeOfActivity;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "position")
    private String position;

    @Column(name = "phone")
    private String phone;

    @Column(name = "time_of_birth")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date timeOfBirth;

    public UserDto entityToUserDto() {
        UserDto userDto = new UserDto();

        userDto.setId(id);

        if (firstName != null){
            userDto.setFirstName(firstName);
        }
        if (lastName != null){
            userDto.setLastName(lastName);
        }
        if (region != null){
            userDto.setRegion(region);
        }
        if (typeOfActivity != null){
            userDto.setTypeOfActivity(typeOfActivity);
        }
        if (companyName != null){
            userDto.setCompanyName(companyName);
        }
        if (position != null){
            userDto.setPosition(position);
        }
        if (timeOfBirth != null){
            userDto.setTimeOfBirth(timeOfBirth);
        }
        if (phone != null){
            userDto.setPhone(phone);
        }

        return userDto;

    }

}
