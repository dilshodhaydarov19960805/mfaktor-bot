package uz.bot.spring.telegram.sarvice;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendVideo;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import uz.bot.spring.telegram.model.dto.UserDto;

import java.util.*;

public interface MyTelegramBotService {

    SendMessage startBot(Update update);

    EditMessageText globalMenu(Update update, Locale locale);

    EditMessageText myProfile(Long chatId, Integer messageId, UserDto dto, Locale locale);

    EditMessageText setTimeOfBirth(Update update, UserDto dto, Locale locale);

    EditMessageText setPosition(Update update, Locale locale);

    EditMessageText setCompanyName(Update update, UserDto dto, Locale locale);

    EditMessageText setTypeOfActivity(Update update, Locale locale);

    EditMessageText setRegion(Update update, Locale locale);

    EditMessageText setLastName(Update update, UserDto dto, Locale locale);

    EditMessageText setFirstName(Update update, UserDto dto, Locale locale);

    EditMessageText setMyPhone(Update update, UserDto dto, Locale locale);

    EditMessageText nextEvents(Update update, Locale locale);

    EditMessageText sendMeeting(Update update, UserDto dto, Locale locale);

    SendVideo sendOurAddress(Long chatId, Locale locale);

    boolean isEmptyUserInfo(UserDto dto);
}
