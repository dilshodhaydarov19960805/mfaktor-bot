package uz.bot.spring.telegram.sarvice;

import uz.bot.spring.telegram.model.domen.Region;

import java.util.Set;

public interface RegionService {

    Set<Region> findAll();

    Region findById(Long id);

}
