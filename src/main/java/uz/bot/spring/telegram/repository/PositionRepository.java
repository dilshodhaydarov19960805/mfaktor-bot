package uz.bot.spring.telegram.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.bot.spring.telegram.model.domen.Position;

@Repository
public interface PositionRepository extends JpaRepository<Position, Long> {

}
