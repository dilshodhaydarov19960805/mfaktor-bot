package uz.bot.spring.telegram.sarvice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.bot.spring.telegram.model.domen.UsersGlobalInformation;
import uz.bot.spring.telegram.model.dto.UsersGlobalInformationDto;
import uz.bot.spring.telegram.repository.UsersGlobalInformationRepository;
import uz.bot.spring.telegram.sarvice.UsersGlobalInformationService;

import java.util.Optional;

@Service
public class UsersGlobalInformationServiceImpl implements UsersGlobalInformationService {

    @Autowired
    private UsersGlobalInformationRepository usersGlobalInformationRepository;

    @Override
    public UsersGlobalInformationDto findById(Integer id) {
        Optional<UsersGlobalInformation> usersGlobalInformation = usersGlobalInformationRepository.findById(id);
        UsersGlobalInformationDto dto = usersGlobalInformation.get().mapToUserInfoDto();
        return dto;
    }

    @Override
    public UsersGlobalInformationDto save(UsersGlobalInformationDto dto) {
        usersGlobalInformationRepository.save(dto.userInfoToEntity());
        return dto;
    }

}
