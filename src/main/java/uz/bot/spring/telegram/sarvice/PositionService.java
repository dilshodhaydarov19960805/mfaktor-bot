package uz.bot.spring.telegram.sarvice;

import uz.bot.spring.telegram.model.domen.Position;

import java.util.Set;

public interface PositionService {

    Set<Position> findAll();

    Position findById(Long id);

}
