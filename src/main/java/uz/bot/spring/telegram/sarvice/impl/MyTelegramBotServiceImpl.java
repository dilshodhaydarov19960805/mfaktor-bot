package uz.bot.spring.telegram.sarvice.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendVideo;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import uz.bot.spring.telegram.model.domen.Position;
import uz.bot.spring.telegram.model.domen.Region;
import uz.bot.spring.telegram.model.domen.TypeOfActivity;
import uz.bot.spring.telegram.model.dto.UserDto;
import uz.bot.spring.telegram.sarvice.*;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Slf4j
public class MyTelegramBotServiceImpl implements MyTelegramBotService {

    @Autowired
    private UsersService usersService;
    @Autowired
    private RegionService regionService;
    @Autowired
    private PositionService positionService;
    @Autowired
    private TypeOfActivityService typeOfActivityService;
    @Autowired
    private MessageSource messageSource;

    private static Set<Region> regions = new HashSet<>();
    private static Set<Position> positions = new HashSet<>();
    private static Set<TypeOfActivity> typeOfActivities = new HashSet<>();

    InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
    List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
    List<InlineKeyboardButton> rowInline = new ArrayList<>();
    SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

    public SendMessage startBot(Update update) {

        long chat_id = 0;
        if (usersService.findById(Long.valueOf(update.getMessage().getFrom().getId())) == null){
            usersService.save(new UserDto(Long.valueOf(update.getMessage().getFrom().getId())));
        }
        chat_id = update.getMessage().getChatId();
        SendMessage message = new SendMessage();
        message.setChatId(chat_id).enableHtml(true)
                .setText("\uD83C\uDDFA\uD83C\uDDFF " + "<b>"+ messageSource.getMessage("select.lang.text", null, new Locale("uz"))+"</b>" + "\n" +"\uD83C\uDDF7\uD83C\uDDFA " + "<b>"+ messageSource.getMessage("select.lang.text", null, new Locale("ru"))+"</b>");
        rowInline.add(new InlineKeyboardButton().setText("\uD83C\uDDFA\uD83C\uDDFF " + "Uz").setCallbackData("/uz"));
        rowInline.add(new InlineKeyboardButton().setText("\uD83C\uDDF7\uD83C\uDDFA " + "Ru").setCallbackData("/ru"));
        rowsInline.add(rowInline);
        markupInline.setKeyboard(rowsInline);
        message.setReplyMarkup(markupInline);
        newRowsInline();

        return message;

    }

    public EditMessageText globalMenu(Update update, Locale locale) {
        UserDto dto = usersService.findById(Long.valueOf(update.getCallbackQuery().getFrom().getId()));
        EditMessageText message = new EditMessageText();
        message.setChatId(update.getCallbackQuery().getMessage().getChatId())
                .setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                .setText( messageSource.getMessage("your.id", null, locale) +
                        update.getCallbackQuery().getFrom().getId() + "\n" + "@ilmi_ziyo_ilimlari_bot " +
                        (isEmptyUserInfo(dto) ? (messageSource.getMessage("user.information.full.text", null, locale)) : "") +
                        "\n\n" + "➡️@ilmi_ziyo_ilimlari_bot " + (!isEmptyUserInfo(dto) ? (messageSource.getMessage("our.channel", null, locale) +
                        "\n" + messageSource.getMessage("your.profile.no.full", null, locale)+"⛔️")
                        : messageSource.getMessage("our.channel", null, locale)));
        rowInline = new ArrayList(Collections.singleton(new InlineKeyboardButton().setText(messageSource.getMessage("my.profile.text", null, locale) + "\uD83D\uDC64").setCallbackData("/uz/my/profile")));
        rowsInline.add(rowInline);
        rowInline = new ArrayList(Collections.singleton(new InlineKeyboardButton().setText(messageSource.getMessage("meeting", null, locale)).setCallbackData("/uz/meeting")));
        rowsInline.add(rowInline);
        rowInline = new ArrayList(Collections.singleton(new InlineKeyboardButton().setText(messageSource.getMessage("connect.with.admin", null, locale)).setUrl("https://t.me/dilshod_bdm_uz")));
        rowsInline.add(rowInline);
        rowInline = new ArrayList(Collections.singleton(new InlineKeyboardButton().setText(messageSource.getMessage("our.address.text", null, locale)).setCallbackData("/uz/us/address")));
        rowsInline.add(rowInline);
        markupInline.setKeyboard(rowsInline);
        message.setReplyMarkup(markupInline);
        newRowsInline();
        return message;
    }

    public EditMessageText setMyPhone(Update update, UserDto dto, Locale locale) {
        EditMessageText message = new EditMessageText()
                .setChatId(update.getCallbackQuery().getMessage().getChatId()).setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                .setText(messageSource.getMessage("your.phone.text", null, locale) + (dto != null && dto.getPhone() != null ? dto.getPhone() : "") + "\n" +
                        messageSource.getMessage("new.your.phone.text", null, locale));
        rowInline.add(new InlineKeyboardButton().setText("⬅️" + messageSource.getMessage("back.up.text", null, locale)).setCallbackData("/uz/my/back/up"));
        rowsInline.add(rowInline);
        markupInline.setKeyboard(rowsInline);
        message.setReplyMarkup(markupInline);
        newRowsInline();
        return message;

    }

    public EditMessageText setFirstName(Update update, UserDto dto, Locale locale) {

        EditMessageText message = new EditMessageText()
                .setChatId(update.getCallbackQuery().getMessage().getChatId()).setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                .setText(messageSource.getMessage("your.last.name.text", null, locale) + (dto != null && dto.getFirstName() != null ? dto.getFirstName() : "") + "\n" +
                        messageSource.getMessage("new.your.last.name.text", null, locale));
        rowInline.add(new InlineKeyboardButton().setText("⬅️" + messageSource.getMessage("back.up.text", null, locale)).setCallbackData("/uz/my/back/up"));
        rowsInline.add(rowInline);
        markupInline.setKeyboard(rowsInline);
        message.setReplyMarkup(markupInline);
        newRowsInline();
        return message;
    }

    public EditMessageText setLastName(Update update, UserDto dto, Locale locale) {
        EditMessageText message = new EditMessageText();
        message.setChatId(update.getCallbackQuery().getMessage().getChatId()).setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                .setText(messageSource.getMessage("your.first.name.text", null, locale) + (dto != null && dto.getLastName() != null ? dto.getLastName() : "") + "\n" +
                        messageSource.getMessage("new.your.first.name.text", null, locale));
        rowInline.add(new InlineKeyboardButton().setText("⬅️" + messageSource.getMessage("back.up.text", null, locale)).setCallbackData("/uz/my/back/up"));
        rowsInline.add(rowInline);
        markupInline.setKeyboard(rowsInline);
        message.setReplyMarkup(markupInline);
        newRowsInline();
        return message;
    }

    public EditMessageText setRegion(Update update, Locale locale) {

        regions = regionService.findAll();
        EditMessageText message = new EditMessageText();
        message.setChatId(update.getCallbackQuery().getMessage().getChatId()).setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                .setText("✅ " +messageSource.getMessage("select.region.text", null, locale)+ " \uD83D\uDC47");
        for (Region region : regions) {
            rowInline = new ArrayList<>();
            rowInline.add(new InlineKeyboardButton().setText(locale.toString().equals("uz") ? region.getRegionNameUz() : region.getRegionNameRu()).setCallbackData("/uz/my/region" + (locale.toString().equals("uz") ? "uz" + region.getId() : "ru" + region.getId())));
            rowsInline.add(rowInline);
        }
        rowInline = new ArrayList();
        rowInline.add(new InlineKeyboardButton().setText("⬅️" + messageSource.getMessage("back.up.text", null, locale)).setCallbackData("/uz/my/back/up"));
        rowsInline.add(rowInline);
        markupInline.setKeyboard(rowsInline);
        message.setReplyMarkup(markupInline);
        newRowsInline();
        return message;

    }

    public EditMessageText setTypeOfActivity(Update update, Locale locale) {

        typeOfActivities = typeOfActivityService.findAll();
        EditMessageText message = new EditMessageText();
        message.setChatId(update.getCallbackQuery().getMessage().getChatId()).setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                .setText("\uD83D\uDD39 " +messageSource.getMessage("select.type.of.activity.text", null, locale)+ " \uD83D\uDC47");
        for (TypeOfActivity typeOfActivity : typeOfActivities ) {
            rowInline = new ArrayList<>();
            rowInline.add(new InlineKeyboardButton().setText("uz".equals(locale.toString()) ? typeOfActivity.getTypeOfActivityNameUz() : typeOfActivity.getTypeOfActivityNameRu()).setCallbackData("/uz/my/type/of/activity" + (locale.toString().equals("uz") ? "uz" + typeOfActivity.getId() : "ru" + typeOfActivity.getId())));
            rowsInline.add(rowInline);
        }
        rowInline = new ArrayList();
        rowInline.add(new InlineKeyboardButton().setText("⬅️" + messageSource.getMessage("back.up.text", null, locale)).setCallbackData("/uz/my/back/up"));
        rowsInline.add(rowInline);
        markupInline.setKeyboard(rowsInline);
        message.setReplyMarkup(markupInline);
        newRowsInline();
        return message;
    }

    public EditMessageText setCompanyName(Update update, UserDto dto, Locale locale) {

        EditMessageText message = new EditMessageText();
        message.setChatId(update.getCallbackQuery().getMessage().getChatId()).setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                .setText(messageSource.getMessage("your.company.name.text", null, locale) + (dto != null && dto.getCompanyName() != null ? dto.getCompanyName() : "") + "\n" +
                        messageSource.getMessage("new.your.company.name.text", null, locale));
        rowInline.add(new InlineKeyboardButton().setText("⬅️" + messageSource.getMessage("back.up.text", null, locale)).setCallbackData("/uz/my/back/up"));
        rowsInline.add(rowInline);
        markupInline.setKeyboard(rowsInline);
        message.setReplyMarkup(markupInline);
        newRowsInline();
        return message;
    }

    public EditMessageText setPosition(Update update, Locale locale) {
        positions = positionService.findAll();
        EditMessageText message = new EditMessageText();
        message.setChatId(update.getCallbackQuery().getMessage().getChatId()).setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                .setText("\uD83D\uDCBC "+messageSource.getMessage("select.position.text", null, locale)+" \uD83D\uDC47");
        for (Position position : positions) {
            rowInline = new ArrayList<>();
            rowInline.add(new InlineKeyboardButton().setText(locale.toString().equals("uz") ? position.getPositionNameUz() : position.getPositionNameRu()).setCallbackData("/uz/my/position" + (locale.toString().equals("uz") ? "uz"+position.getId() : "ru"+position.getId())));
            rowsInline.add(rowInline);
        }
        rowInline = new ArrayList();
        rowInline.add(new InlineKeyboardButton().setText("⬅️" + messageSource.getMessage("back.up.text", null, locale)).setCallbackData("/uz/my/back/up"));
        rowsInline.add(rowInline);
        markupInline.setKeyboard(rowsInline);
        message.setReplyMarkup(markupInline);
        newRowsInline();
        return message;
    }

    public EditMessageText setTimeOfBirth(Update update, UserDto dto, Locale locale) {
        EditMessageText message = new EditMessageText();
        message.setChatId(update.getCallbackQuery().getMessage().getChatId()).setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                .setText(messageSource.getMessage("your.time.of.birth", null, locale) + (dto != null && dto.getTimeOfBirth() != null ? format.format(dto.getTimeOfBirth()) : "") + "\n" +
                        messageSource.getMessage("your.time.of.birth.text.example", null, locale));
        rowInline.add(new InlineKeyboardButton().setText("⬅️" + messageSource.getMessage("back.up.text", null, locale)).setCallbackData("/uz/my/back/up"));
        rowsInline.add(rowInline);
        markupInline.setKeyboard(rowsInline);
        message.setReplyMarkup(markupInline);
        newRowsInline();
        return message;

    }

    public EditMessageText nextEvents(Update update, Locale locale) {
        EditMessageText message = new EditMessageText()
                .setChatId(update.getCallbackQuery().getMessage().getChatId())
                .setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                .setText(messageSource.getMessage("next.events.text", null, locale)+ "\n\n" +
                        "MFP-65." + "Alisher  Rustamov" +
                        " \uD83D\uDD39 " + "Савдо-сотиқдаги этирозлар ва низолар билан ишлаш" +
                        " \uD83D\uDD54" + "24.04.2021 11:00");
        rowInline = new ArrayList(Collections.singleton(new InlineKeyboardButton().setText("Савдо-сотиқдаги этирозлар ва низолар билан ишлаш").setCallbackData("/uz/my/new/next/events")));
        rowsInline.add(rowInline);
        rowInline = new ArrayList(Collections.singleton(new InlineKeyboardButton().setText("⬅️" + messageSource.getMessage("back.up.text", null, locale)).setCallbackData("/uz/meeting")));
        rowsInline.add(rowInline);
        markupInline.setKeyboard(rowsInline);
        message.setReplyMarkup(markupInline);
        newRowsInline();
        return message;
    }

    public SendVideo sendOurAddress(Long chatId, Locale locale) {

        File file = new File("C:\\Users\\dilshod\\Videos\\Movavi Screen Recorder\\video.mp4");
        SendVideo message = new SendVideo();
        message.setChatId(chatId).enableNotification().
                setCaption("\uD83C\uDFE2 " +messageSource.getMessage("our.address.video.text", null, locale) +
                        "\n\n" + "\uD83D\uDCCD " + messageSource.getMessage("destined.for.our.address.text", null, locale)).setParseMode(ParseMode.HTML).
                setVideo(file);
        rowInline = new ArrayList(Collections.singleton(new InlineKeyboardButton().setText("⬅️" + messageSource.getMessage("back.up.text", null, locale)).setCallbackData("/uz/back/up")));
        rowsInline.add(rowInline);
        markupInline.setKeyboard(rowsInline);
        message.setReplyMarkup(markupInline);
        newRowsInline();
        return message;

    }

    public EditMessageText sendMeeting(Update update, UserDto dto, Locale locale) {

        EditMessageText message = new EditMessageText();
        message.setChatId(update.getCallbackQuery().getMessage().getChatId())
                .setMessageId(update.getCallbackQuery().getMessage().getMessageId());
        if (isEmptyUserInfo(dto)){
            message .setText(messageSource.getMessage("meaningful.words.text", null, locale));
            rowInline = new ArrayList(Collections.singleton(new InlineKeyboardButton().setText(messageSource.getMessage("next.events.text", null, locale)).setCallbackData("/uz/my/next/events")));
            rowsInline.add(rowInline);
            rowInline = new ArrayList(Collections.singleton(new InlineKeyboardButton().setText(messageSource.getMessage("registered.activities.text", null, locale)).setCallbackData("/uz/my/registered/activities")));
            rowsInline.add(rowInline);
        }else {
            message.setText(messageSource.getMessage("your.profile.information.no.full.text", null, locale));
            rowInline = new ArrayList(Collections.singleton(new InlineKeyboardButton().setText(messageSource.getMessage("my.profile.text", null, locale) + "\uD83D\uDC64").setCallbackData("/uz/my/profile")));
            rowsInline.add(rowInline);
        }
        rowInline = new ArrayList(Collections.singleton(new InlineKeyboardButton().setText("⬅️" + messageSource.getMessage("back.up.text", null, locale)).setCallbackData("/uz/back/up")));
        rowsInline.add(rowInline);
        markupInline.setKeyboard(rowsInline);
        message.setReplyMarkup(markupInline);
        newRowsInline();
        return message;

    }

    public EditMessageText myProfile(Long chatId, Integer messageId, UserDto dto, Locale locale){
        EditMessageText message = new EditMessageText()
                .setChatId(chatId).setMessageId(messageId)
                .setText(messageSource.getMessage("my.informations.text", null, locale) + "\n" + "\n" +
                        (dto != null ? ((dto.getPhone() != null ? "✅️" :"❗️") + messageSource.getMessage("my.information.phone.text", null, locale) + ": " + (dto.getPhone() != null ? dto.getPhone() : messageSource.getMessage("my.no.information.text", null, locale)) + "\n" +
                                (dto.getFirstName() != null ? "✅️" :"❗️") + messageSource.getMessage("my.information.first.name.text", null, locale) + ": " + (dto.getFirstName() != null ? dto.getFirstName() : messageSource.getMessage("my.no.information.text", null, locale)) + "\n" +
                                (dto.getLastName() != null ? "✅️" :"❗️") + messageSource.getMessage("my.information.last.name.text", null, locale) + ": " + (dto.getLastName() != null ? dto.getLastName() : messageSource.getMessage("my.no.information.text", null, locale)) + "\n" +
                                (dto.getRegion() != null ? "✅️" :"❗️") + messageSource.getMessage("my.information.region.text", null, locale) + ": " + (dto.getRegion() != null ? dto.getRegion() : messageSource.getMessage("my.no.information.text", null, locale)) + "\n" +
                                (dto.getTypeOfActivity() != null ? "✅️" :"❗️") + messageSource.getMessage("my.information.type.of.activity.text", null, locale) + ": " + (dto.getTypeOfActivity() != null ? dto.getTypeOfActivity() : messageSource.getMessage("my.no.information.text", null, locale)) + "\n" +
                                (dto.getCompanyName() != null ? "✅️" :"❗️") + messageSource.getMessage("my.information.company.name.text", null, locale) + ": " + (dto.getCompanyName() != null ? dto.getCompanyName() : messageSource.getMessage("my.no.information.text", null, locale)) + "\n" +
                                (dto.getPosition() != null ? "✅️" :"❗️") + messageSource.getMessage("my.information.position.text", null, locale) + ": " + (dto.getPosition() != null ? dto.getPosition() : messageSource.getMessage("my.no.information.text", null, locale)) + "\n" +
                                (dto.getTimeOfBirth() != null ? "✅️" :"❗️") + messageSource.getMessage("my.information.time.of.birth.text", null, locale) + ": " + (dto.getTimeOfBirth() != null ? format.format(dto.getTimeOfBirth()) : messageSource.getMessage("my.no.information.text", null, locale))
                        ) :
                                "❗️" + messageSource.getMessage("my.information.phone.text", null, locale) + ": " + messageSource.getMessage("my.no.information.text", null, locale) + "\n" +
                                        "❗️" + messageSource.getMessage("my.information.first.name.text", null, locale) + ": " + messageSource.getMessage("my.no.information.text", null, locale) + "\n" +
                                        "❗️" + messageSource.getMessage("my.information.last.name.text", null, locale) + ": " + messageSource.getMessage("my.no.information.text", null, locale) + "\n" +
                                        "❗️" + messageSource.getMessage("my.information.region.text", null, locale) + ": " + messageSource.getMessage("my.no.information.text", null, locale) + "\n" +
                                        "❗️" + messageSource.getMessage("my.information.type.of.activity.text", null, locale) + ": " + messageSource.getMessage("my.no.information.text", null, locale) + "\n" +
                                        "❗️" + messageSource.getMessage("my.information.company.name.text", null, locale) + ": " + messageSource.getMessage("my.no.information.text", null, locale) + "\n" +
                                        "❗️" + messageSource.getMessage("my.information.position.text", null, locale) + ": " + messageSource.getMessage("my.no.information.text", null, locale) + "\n" +
                                        "❗️" + messageSource.getMessage("my.information.time.of.birth.text", null, locale) + ": " + messageSource.getMessage("my.no.information.text", null, locale))
                );
        rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("my.information.phone.text", null, locale) + " " + "\uD83D\uDCDD").setCallbackData("/uz/my/phone"));
        rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("my.information.first.name.text", null, locale) + " " + "\uD83D\uDCDD").setCallbackData("/uz/my/first/name"));
        rowsInline.add(rowInline);
        rowInline = new ArrayList();
        rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("my.information.last.name.text", null, locale) + " " + "\uD83D\uDCDD").setCallbackData("/uz/my/last/name"));
        rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("my.information.region.text", null, locale) + " " + "\uD83D\uDCDD").setCallbackData("/uz/my/region"));
        rowsInline.add(rowInline);
        rowInline = new ArrayList();
        rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("my.information.type.of.activity.text", null, locale) + " " + "\uD83D\uDCDD").setCallbackData("/uz/my/type/of/activity"));
        rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("my.information.company.name.text", null, locale) + " " + "\uD83D\uDCDD").setCallbackData("/uz/my/company/name"));
        rowsInline.add(rowInline);
        rowInline = new ArrayList();
        rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("my.information.position.text", null, locale) + " " + "\uD83D\uDCDD").setCallbackData("/uz/my/position"));
        rowInline.add(new InlineKeyboardButton().setText(messageSource.getMessage("my.information.time.of.birth.text", null, locale) + " " + "\uD83D\uDCDD").setCallbackData("/uz/my/time/of/birth"));
        rowsInline.add(rowInline);
        rowInline = new ArrayList(Collections.singleton(new InlineKeyboardButton().setText("⬅️" + messageSource.getMessage("back.up.text", null, locale)).setCallbackData("/uz/back/up")));
        rowsInline.add(rowInline);
        markupInline.setKeyboard(rowsInline);
        message.setReplyMarkup(markupInline);
        newRowsInline();
        return message;
    }

    public boolean isEmptyUserInfo(UserDto userDto) {

        if (userDto != null && userDto.getCompanyName() != null && userDto.getFirstName() !=null && userDto.getLastName() != null && userDto.getPosition() != null && userDto.getRegion() != null && userDto.getTimeOfBirth() != null && userDto.getTypeOfActivity() != null && userDto.getPhone() != null){
            return true;
        }

        return false;

    }

    private void newRowsInline() {

        rowInline = new ArrayList<>();
        markupInline = new InlineKeyboardMarkup();
        rowsInline = new ArrayList<>();

    }

}