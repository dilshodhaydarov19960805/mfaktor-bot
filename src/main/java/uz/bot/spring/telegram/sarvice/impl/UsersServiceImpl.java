package uz.bot.spring.telegram.sarvice.impl;

import org.springframework.stereotype.Service;
import uz.bot.spring.telegram.model.domen.Users;
import uz.bot.spring.telegram.model.dto.UserDto;
import uz.bot.spring.telegram.repository.UserRepository;
import uz.bot.spring.telegram.sarvice.UsersService;

import java.util.Optional;

@Service
public class UsersServiceImpl implements UsersService {


    private final UserRepository userRepository;

    public UsersServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDto findById(Long id) {
        Optional<Users> users =  userRepository.findById(id);
        return users.map(Users::entityToUserDto).orElse(null);
    }

    public UserDto save(UserDto userDto) {

        Users user = userDto.userDtoToEntity();
        userRepository.save(user);
        return userDto;
    }

}
