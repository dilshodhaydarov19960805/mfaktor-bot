package uz.bot.spring.telegram.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.bot.spring.telegram.model.domen.UsersGlobalInformation;

@Repository
public interface UsersGlobalInformationRepository extends JpaRepository<UsersGlobalInformation, Integer> {


}
