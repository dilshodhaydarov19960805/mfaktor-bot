package uz.bot.spring.telegram.sarvice;

import uz.bot.spring.telegram.model.dto.UserDto;

public interface UsersService {

    UserDto findById(Long id);

    UserDto save(UserDto userDto);

}
