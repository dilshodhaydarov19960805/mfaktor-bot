package uz.bot.spring.telegram.model.domen;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.bot.spring.telegram.model.dto.UsersGlobalInformationDto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Locale;

@Entity
@Table(name = "users_global_information")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UsersGlobalInformation implements Serializable {

    @Id
    @Column(name = "user_info_id")
    private Integer userId;

    @Column(name = "lang")
    private Locale lang;

    @Column(name = "lang_transaction_i")
    private int transactionLangI;

    @Column(name = "callback_query_id")
    private String callbackQueryId;

    @Column(name = "my_message_id")
    private Integer myMessageId;

    @Column(name = "global_id")
    private Long globalId;

    public UsersGlobalInformationDto mapToUserInfoDto() {
        UsersGlobalInformationDto dto = new UsersGlobalInformationDto();

        dto.setUserId(userId);

        if (lang != null){
            dto.setLang(lang);
        }

        dto.setTransactionLangI(transactionLangI);

        if (callbackQueryId != null){
            dto.setCallbackQueryId(callbackQueryId);
        }
        if (myMessageId != null){
            dto.setMyMessageId(myMessageId);
        }
        if (globalId != null){
            dto.setGlobalId(globalId);
        }

        return dto;
    }

}
