package uz.bot.spring.telegram.model.domen;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "type_of_activity")
public class TypeOfActivity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_of_activity_id")
    private Long id;

    @Column(name = "type_of_activity_name_uz")
    private String typeOfActivityNameUz;

    @Column(name = "type_of_activity_name_ru")
    private String typeOfActivityNameRu;

}
