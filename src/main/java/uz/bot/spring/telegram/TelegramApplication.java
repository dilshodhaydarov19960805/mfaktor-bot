package uz.bot.spring.telegram;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import uz.bot.spring.telegram.model.MyTelegramBot;

@SpringBootApplication
public class TelegramApplication implements CommandLineRunner {

	@Autowired
	MyTelegramBot myTelegramBot;

	public static void main(String[] args) {
		ApiContextInitializer.init();
		SpringApplication.run(TelegramApplication.class, args);
	}

	@Override
	public void run(String... args) throws TelegramApiRequestException {

		ApiContextInitializer.init();
		TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
		telegramBotsApi.registerBot(myTelegramBot);

	}

}
