package uz.bot.spring.telegram.sarvice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.bot.spring.telegram.model.domen.TypeOfActivity;
import uz.bot.spring.telegram.repository.TypeOfActivityRepository;
import uz.bot.spring.telegram.sarvice.TypeOfActivityService;

import java.util.HashSet;
import java.util.Set;

@Service
public class TypeOfActivityServiceImpl implements TypeOfActivityService {

    @Autowired
    private TypeOfActivityRepository typeOfActivityRepository;

    public Set<TypeOfActivity> findAll(){

        Set<TypeOfActivity> typeOfActivities = new HashSet<>();
        typeOfActivities.addAll(typeOfActivityRepository.findAll());

        return typeOfActivities;

    }

    public TypeOfActivity findById(Long id) {

        return typeOfActivityRepository.findById(id).get();

    }

}
