package uz.bot.spring.telegram.model;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendLocation;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.bot.spring.telegram.model.domen.Position;
import uz.bot.spring.telegram.model.domen.Region;
import uz.bot.spring.telegram.model.domen.TypeOfActivity;
import uz.bot.spring.telegram.model.dto.BotConfig;
import uz.bot.spring.telegram.model.dto.UserDto;
import uz.bot.spring.telegram.model.dto.UsersGlobalInformationDto;
import uz.bot.spring.telegram.sarvice.*;

import java.text.SimpleDateFormat;
import java.util.*;

@Component
@Slf4j
public class MyTelegramBot extends TelegramLongPollingBot {

    @Autowired
    private UsersService usersService;
    @Autowired
    private PositionService positionService;
    @Autowired
    TypeOfActivityService typeOfActivityService;
    @Autowired
    RegionService regionService;
    @Autowired
    private MyTelegramBotService myTelegramBotService;
    @Autowired
    private UsersGlobalInformationService usersGlobalInformationService;

    private UsersGlobalInformationDto usersGlobalInformationDto;

    @SneakyThrows
    @Override
    public void onUpdateReceived(Update update) {
        if (update.getMessage() != null && update.getMessage().hasText() && "/start".equals(update.getMessage().getText())){
            usersGlobalInformationDto = new UsersGlobalInformationDto();
            usersGlobalInformationDto.setTransactionLangI(1);
            usersGlobalInformationDto.setUserId(update.getMessage().getFrom().getId());
            usersGlobalInformationService.save(usersGlobalInformationDto);
            try {
                execute(myTelegramBotService.startBot(update));
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }

        }else if (update.hasCallbackQuery()){
            UserDto dto = usersService.findById(Long.valueOf(update.getCallbackQuery().getFrom().getId()));
            usersGlobalInformationDto = usersGlobalInformationService.findById(update.getCallbackQuery().getFrom().getId());
            if (usersGlobalInformationDto.getTransactionLangI() == 1){
                if ("/uz".equals(update.getCallbackQuery().getData())){
                    usersGlobalInformationDto.setLang(new Locale("uz"));
                }else {
                    usersGlobalInformationDto.setLang(new Locale("ru"));
                }
                usersGlobalInformationDto.setTransactionLangI(0);
                usersGlobalInformationService.save(usersGlobalInformationDto);
            }
            String api = update.getCallbackQuery().getData().length() > 3 ? update.getCallbackQuery().getData().substring(0, 6) : update.getCallbackQuery().getData();
            if ((!"/uz/my".equals(api) && !"/uz/me".equals(api) && !"/uz/us".equals(api)) || "/uz/back/up".equals(update.getCallbackQuery().getData())){
//                globalId = update.getCallbackQuery().getMessage().getChatId();
                sendExecute(myTelegramBotService.globalMenu(update, usersGlobalInformationDto.getLang()));
            }else if ("/uz/meeting".equals(update.getCallbackQuery().getData())){
                sendExecute(myTelegramBotService.sendMeeting(update, dto, usersGlobalInformationDto.getLang()));
            }else if ("/uz/my/next/events".equals(update.getCallbackQuery().getData())){
                sendExecute(myTelegramBotService.nextEvents(update, usersGlobalInformationDto.getLang()));
            }else if("/uz/us/address".equals(update.getCallbackQuery().getData())){
                try {
                    execute(new SendLocation().setChatId(update.getCallbackQuery().getMessage().getChatId()).setLatitude(41.324354f).setLongitude(69.310470f).setLivePeriod(60));
                    execute(myTelegramBotService.sendOurAddress(update.getCallbackQuery().getMessage().getChatId(), usersGlobalInformationDto.getLang()));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }else if ("/uz/my/profile".equals(update.getCallbackQuery().getData()) || "/uz/my/back/up".equals(update.getCallbackQuery().getData())){
                usersGlobalInformationDto.setMyMessageId(update.getCallbackQuery().getMessage().getMessageId());
                usersGlobalInformationService.save(usersGlobalInformationDto);
                sendExecute(myTelegramBotService.myProfile(Long.valueOf(usersGlobalInformationDto.getUserId()), usersGlobalInformationDto.getMyMessageId(), dto, usersGlobalInformationDto.getLang()));
            }else if ("/uz/my/phone".equals(update.getCallbackQuery().getData())){
                usersGlobalInformationDto.setCallbackQueryId("ph" + update.getCallbackQuery().getFrom().getId());
                usersGlobalInformationService.save(usersGlobalInformationDto);
                sendExecute(myTelegramBotService.setMyPhone(update, dto, usersGlobalInformationDto.getLang()));
            }else if ("/uz/my/first/name".equals(update.getCallbackQuery().getData())){
                usersGlobalInformationDto.setCallbackQueryId("fn" + update.getCallbackQuery().getFrom().getId());
                usersGlobalInformationService.save(usersGlobalInformationDto);
                sendExecute(myTelegramBotService.setFirstName(update, dto, usersGlobalInformationDto.getLang()));
            }else if ("/uz/my/last/name".equals(update.getCallbackQuery().getData())){
                usersGlobalInformationDto.setCallbackQueryId("ln" + update.getCallbackQuery().getFrom().getId());
                usersGlobalInformationService.save(usersGlobalInformationDto);
                sendExecute(myTelegramBotService.setLastName(update, dto, usersGlobalInformationDto.getLang()));
            }else if ("/uz/my/region".equals(update.getCallbackQuery().getData())){
                sendExecute(myTelegramBotService.setRegion(update, usersGlobalInformationDto.getLang()));
            }else if ("/uz/my/type/of/activity".equals(update.getCallbackQuery().getData())){
                sendExecute(myTelegramBotService.setTypeOfActivity(update, usersGlobalInformationDto.getLang()));
            }else if ("/uz/my/company/name".equals(update.getCallbackQuery().getData())){
                usersGlobalInformationDto.setCallbackQueryId("cn" + update.getCallbackQuery().getFrom().getId());
                usersGlobalInformationService.save(usersGlobalInformationDto);
                sendExecute(myTelegramBotService.setCompanyName(update, dto, usersGlobalInformationDto.getLang()));
            }else if ("/uz/my/position".equals(update.getCallbackQuery().getData())){
                sendExecute(myTelegramBotService.setPosition(update, usersGlobalInformationDto.getLang()));
            }else  if ("/uz/my/time/of/birth".equals(update.getCallbackQuery().getData())){
                usersGlobalInformationDto.setCallbackQueryId("tb" + update.getCallbackQuery().getFrom().getId());
                usersGlobalInformationService.save(usersGlobalInformationDto);
                sendExecute(myTelegramBotService.setTimeOfBirth(update, dto, usersGlobalInformationDto.getLang()));
            }else if (update.getCallbackQuery().getData().startsWith("region",7)){
                Region region = regionService.findById(Long.valueOf(update.getCallbackQuery().getData().substring(15)));
                String lang = update.getCallbackQuery().getData().substring(13,15);
                if ("uz".equals(lang)){
                    dto.setRegion(region.getRegionNameUz());
                }else
                    dto.setRegion(region.getRegionNameRu());
                usersService.save(dto);
                sendExecute(myTelegramBotService.myProfile(update.getCallbackQuery().getMessage().getChatId(), update.getCallbackQuery().getMessage().getMessageId(), dto, usersGlobalInformationDto.getLang()));
            }else if (update.getCallbackQuery().getData().startsWith("position",7)){
                Position position = positionService.findById(Long.valueOf(update.getCallbackQuery().getData().substring(17)));
                String lang = update.getCallbackQuery().getData().substring(15, 17);
                if ("uz".equals(lang)){
                    dto.setPosition(position.getPositionNameUz());
                }else
                    dto.setPosition(position.getPositionNameRu());
                usersService.save(dto);
                sendExecute(myTelegramBotService.myProfile(update.getCallbackQuery().getMessage().getChatId(), update.getCallbackQuery().getMessage().getMessageId(), dto, usersGlobalInformationDto.getLang()));
            }else if (update.getCallbackQuery().getData().startsWith("activity",15)){
                TypeOfActivity typeOfActivity = typeOfActivityService.findById(Long.valueOf(update.getCallbackQuery().getData().substring(25)));
                String lang = update.getCallbackQuery().getData().substring(23, 25);
                if ("uz".equals(lang)){
                    dto.setTypeOfActivity(typeOfActivity.getTypeOfActivityNameUz());
                }else
                    dto.setTypeOfActivity(typeOfActivity.getTypeOfActivityNameRu());
                usersService.save(dto);
                sendExecute(myTelegramBotService.myProfile(update.getCallbackQuery().getMessage().getChatId(), update.getCallbackQuery().getMessage().getMessageId(), dto, usersGlobalInformationDto.getLang()));
            }
        }else if (update != null && update.getMessage().hasText()){
            usersGlobalInformationDto = usersGlobalInformationService.findById(update.getMessage().getFrom().getId());
            String value = usersGlobalInformationDto.getCallbackQueryId().substring(0, 2);
            int messageId = Integer.parseInt(usersGlobalInformationDto.getCallbackQueryId().substring(2));
            if (messageId == update.getMessage().getFrom().getId()){
                UserDto dto = usersService.findById(Long.valueOf(update.getMessage().getFrom().getId()));
                if (dto != null){
                    if ("ph".equals(value)){
                        dto.setPhone(update.getMessage().getText());
                    }else if ("fn".equals(value)){
                        dto.setFirstName(update.getMessage().getText());
                    }else if ("ln".equals(value)){
                        dto.setLastName(update.getMessage().getText());
                    }else if ("tb".equals(value)){
                        dto.setTimeOfBirth(new SimpleDateFormat("dd.MM.yyyy").parse(update.getMessage().getText()));
                    }else if ("cn".equals(value)){
                        dto.setCompanyName(update.getMessage().getText());
                    }
                    usersService.save(dto);
                    usersGlobalInformationDto.setCallbackQueryId(null);
                    usersGlobalInformationService.save(usersGlobalInformationDto);
                }
                sendExecute(myTelegramBotService.myProfile(Long.valueOf(usersGlobalInformationDto.getUserId()), usersGlobalInformationDto.getMyMessageId(), dto, usersGlobalInformationDto.getLang()));
            }
        }

    }

    private void sendExecute(EditMessageText message){
        try {
            execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getBotUsername() {
        return BotConfig.BOT_USER_NAME;
    }

    @Override
    public String getBotToken() {
        return BotConfig.BOT_TOKEN;
    }

}