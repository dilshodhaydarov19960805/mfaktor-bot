//package uz.bot.spring.telegram;
//
//import uz.bot.spring.telegram.model.dto.Person;
//
//import java.lang.reflect.Array;
//import java.util.*;
//import java.util.function.IntUnaryOperator;
//import java.util.stream.Stream;
//
//public class StringMain {
//
//    public static void main(String[] args) {
//        Person p[] = { new Person("Sardor", 25),
//                new Person("Dilshod", 25), new Person("Ismoil", 23),
//                new Person("Bahodir", 23), new Person("Akbar", 20) };
////        int[] a = new int[]{12,23,12};
////        char[] c = new char[]{'1','e','d'};
//
////         <------------toString()------------>
////        boolean[] boolArr
////                = new boolean[] { true, true, false, true };
////        char[] charArr
////                = new char[] { 'g', 'e', 'e', 'k', 's' };
////        double[] dblArr = new double[4];
////        dblArr[0] = 1;
////        dblArr[1] = 2;
////        dblArr[2] = 3;
////        int[] intArr
////                = new int[] { 1, 2, 3, 4 };
////        Object[] objArr
////                = new Object[] { 1, 2, 3, 4 };
////
////        System.out.println(
////                "Boolean Array: "
////                        + Arrays.toString(boolArr));
////        System.out.println(
////                "Character Array: "
////                        + Arrays.toString(charArr));
////        System.out.println(
////                "Double Array: "
////                        + Arrays.toString(dblArr));
////        System.out.println(
////                "Integer Array: "
////                        + Arrays.toString(intArr));
////        System.out.println(
////                "Object Array: "
////                        + Arrays.toString(objArr));
//
////        <----------------Arrays.deepToString()------------->
//
////        String[] strs = new String[] {"practice.geeksforgeeks.org",
////                "quiz.geeksforgeeks.org"
////        };
////        System.out.println(Arrays.deepToString(strs));
////
////        Integer [][]  arr1 = {{10, 20, 30, 40},{12,23,34,34}};
////        System.out.println(Arrays.asList(arr1));
//
////        try {
//
////            // creating Arrays of Integer type
////            Integer a[] = new Integer[] { 10, 20, 30, 0,-1 };
////            double floatArr[] = {10.2f,15.1f,2.2f,3.5f};
////            double f = 4f;
////
////            // getting the list view of Array
////            List<Integer> list = Arrays.asList(a);
////            Arrays.sort(a);
////            Arrays.sort(floatArr);
////            a[3]++;
////            System.out.println(Arrays.binarySearch(a,54));
////            System.out.println(Arrays.binarySearch(floatArr,f));
////            // printing the list
////            System.out.println("The list is: " + list);
////            System.out.println("The list is: " + Arrays.toString(a));
////        }
////
////        catch (NullPointerException e) {
////            System.out.println("Exception thrown : " + e);
////        }
//
//
////        <-------------------binarySearch()--------------->
////
////        byte byteArr[] = {10,20,15,22,35};
////        char charArr[] = {'g','p','q','c','i'};
////        int intArr[] = {10,20,15,22,35};
////        double doubleArr[] = {10.2,15.1,2.2,3.5};
////        float floatArr[] = {10.2f,15.1f,2.2f,3.5f};
////        short shortArr[] = {10,20,15,22,35};
////
////        Arrays.sort(byteArr);
////        Arrays.sort(charArr);
////        Arrays.sort(intArr);
////        Arrays.sort(doubleArr);
////        Arrays.sort(floatArr);
////        Arrays.sort(shortArr);
////
////        byte byteKey = 35;
////        char charKey = 'a';
////        int intKey = 22;
////        double doubleKey = 1.5;
////        float floatKey = 35;
////        short shortKey = 5;
////
////        System.out.println(byteKey + " found at index = "
////                +Arrays.binarySearch(byteArr,byteKey));
////        System.out.println(charKey + " found at index = "
////                +Arrays.binarySearch(charArr,charKey));
////        System.out.println(intKey + " found at index = "
////                +Arrays.binarySearch(intArr,intKey));
////        System.out.println(doubleKey + " found at index = "
////                +Arrays.binarySearch(doubleArr,doubleKey));
////        System.out.println(floatKey + " found at index = "
////                +Arrays.binarySearch(floatArr,floatKey));
////        System.out.println(shortKey + " found at index = "
////                +Arrays.binarySearch(shortArr,shortKey));
//
////        <---------------HashCode()------------>
//
////        System.out.println(Arrays.hashCode(new char[]{'1','e','d'}));
////        Arrays.fill(a, 1);
////        System.out.println(Arrays.toString(a));
//
////        <------------setAll() vs parallelSetAll()-------------->
//
////        IntUnaryOperator operator = s ->{
////          if (s % 2 == 0 )
////              return s * s;
////          else
////              return s;
////        };
////        System.out.println(operator);
////        Arrays.setAll(a, operator);
////        System.out.println(Arrays.toString(a));
////        Arrays.stream(a).forEach(e->{
////            System.out.println(e);
////        });
//        Arrays.setAll(p, e->{
//            if (p[e].getName().startsWith("D"))
//                return new Person("Siz", 100);
//            else
//                return new Person(p[e].getName(), p[e].getAge());
//        });
//        System.out.println("setAll() -> misol");
//        Arrays.stream(p).forEach(e->System.out.println(e.getAge() + "   " + e.getName()));
//
//        Arrays.parallelSetAll(p, e->{
//            if (p[e].getName().startsWith("S")) {
//                return new Person("Siz", 100);
//            }
//            else {
//                return new Person(p[e].getName(), p[e].getAge());
//            }
//        });
//
//        System.out.println("parallelSetAll() -> misol");
//        Arrays.stream(p).forEach(s->System.out.println(s.getAge() + "   " + s.getName()));
//
//        //  <--------------ParallelPrefix()--------------->
////            int[] arry = { 2, 1, 7, 8, 4, 5, 6, 9, 8, 7, 1, 2, 3, 6, 5, 4, 7, 5 };
////            Arrays.parallelPrefix(arry, Integer::sum);
////            Arrays.stream(arry).forEach(e -> System.out.print(e + "   "));
//        int[] arr2 = { 1, 2, 4, 8, 5, 9, 6, 8, 9, 10, 11 };
//        Arrays.parallelPrefix(arr2, 2, 8, (x, y) -> {
//            if (x % 2 == 0 && y % 2 == 0)
//                return x;
//            else
//                return y;
//        });
//        Arrays.stream(arr2).forEach(e -> System.out.print(e + "   "));
//
//
////            <------------Stream()----------->\
//
////        String[] arrs = { "Geeks", "for", "Geeks" };
////        Stream<String> stream = Arrays.stream(arrs);
////
////        stream.forEach(str -> System.out.print(str + " "));
//
////            <------------Spliterator()----------->\
//
////        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9};
////        Spliterator<Integer> s1 = Arrays.spliterator(arr, 0, 4);
//////        Spliterator<Integer> s2 = s1.trySplit();
////        System.out.println("spliterator 1");
////        s1.forEachRemaining(System.out::println);
////        System.out.println("spliterator 2");
//////        s2.forEachRemaining(System.out::println);
//
//    }
//
//}
