package uz.bot.spring.telegram.sarvice;


import uz.bot.spring.telegram.model.dto.UsersGlobalInformationDto;

public interface UsersGlobalInformationService {

    UsersGlobalInformationDto findById(Integer id);

    UsersGlobalInformationDto save(UsersGlobalInformationDto dto);

}
