package uz.bot.spring.telegram.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import uz.bot.spring.telegram.model.domen.Users;

import java.util.Date;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

    private Long id;

    private String firstName;

    private String lastName;

    private String region;

    private String typeOfActivity;

    private String companyName;

    private String position;

    private Date timeOfBirth;

    private String phone;

    public UserDto(Long id) {
        this.id = id;
    }

    public Users userDtoToEntity() {
        Users user = new Users();

        user.setId(id);

        if (firstName != null){
            user.setFirstName(firstName);
        }
        if (lastName != null){
            user.setLastName(lastName);
        }
        if (region != null){
            user.setRegion(region);
        }
        if (typeOfActivity != null){
            user.setTypeOfActivity(typeOfActivity);
        }
        if (companyName != null){
            user.setCompanyName(companyName);
        }
        if (position != null){
            user.setPosition(position);
        }
        if (timeOfBirth != null){
            user.setTimeOfBirth(timeOfBirth);
        }

        if (phone != null){
            user.setPhone(phone);
        }

        return user;

    }

}
