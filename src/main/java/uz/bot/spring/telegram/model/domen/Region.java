package uz.bot.spring.telegram.model.domen;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Setter
@Getter
@Table(name = "region")
public class Region implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "region_id")
    private Long id;

    @Column(name = "region_name_uz")
    private String regionNameUz;

    @Column(name = "region_name_ru")
    private String regionNameRu;

}
