package uz.bot.spring.telegram.sarvice;

import uz.bot.spring.telegram.model.domen.TypeOfActivity;

import java.util.Set;

public interface TypeOfActivityService {

    Set<TypeOfActivity> findAll();

    TypeOfActivity findById(Long id);

}
